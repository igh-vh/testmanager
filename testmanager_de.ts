<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en_GB">
<context>
    <name>AboutDialog</name>
    <message>
        <location filename="AboutDialog.ui" line="14"/>
        <source>About Testmanager</source>
        <translation>Über den Testmanager</translation>
    </message>
    <message>
        <location filename="AboutDialog.ui" line="51"/>
        <source>Testmanager – Next Generation</source>
        <translation>Testmanager – Next Generation</translation>
    </message>
    <message>
        <location filename="AboutDialog.ui" line="61"/>
        <source>EtherLab&apos;s Open-Source Automation and Visualisation Tool, based on the work of Dr.-Ing. Wilhelm Hagemeister.</source>
        <translation>Freies Automatisierungs- und Visualisierungs-Werkzeug der EtherLab-Technologie, basierend auf der Arbeit von Dr.-Ing. Wilhelm Hagemeister.</translation>
    </message>
    <message>
        <location filename="AboutDialog.ui" line="96"/>
        <source>http://etherlab.org/en/testmanager</source>
        <translation>http://etherlab.org/de/testmanager</translation>
    </message>
</context>
<context>
    <name>BarPlugin</name>
    <message>
        <location filename="plugins/BarPlugin.cpp" line="39"/>
        <source>Bar</source>
        <translation>Balken</translation>
    </message>
    <message>
        <location filename="plugins/BarPlugin.cpp" line="107"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>CheckBoxPlugin</name>
    <message>
        <location filename="plugins/CheckBoxPlugin.cpp" line="39"/>
        <source>CheckBox</source>
        <translation>Markierungsfeld</translation>
    </message>
    <message>
        <location filename="plugins/CheckBoxPlugin.cpp" line="109"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>ConnectDialog</name>
    <message>
        <location filename="ConnectDialog.ui" line="20"/>
        <source>Connect...</source>
        <translation>Verbinden...</translation>
    </message>
    <message>
        <location filename="ConnectDialog.ui" line="29"/>
        <source>Data so&amp;urce URL:</source>
        <translation>&amp;Datenquellen-URL:</translation>
    </message>
    <message>
        <source>Replace &amp;permanently</source>
        <translation type="obsolete">&amp;Dauerhaft ersetzen</translation>
    </message>
    <message>
        <source>Rep&amp;lace during this session only</source>
        <translation type="obsolete">Nur in dieser &amp;Sitzung ersetzen</translation>
    </message>
    <message>
        <location filename="ConnectDialog.ui" line="82"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ConnectDialog.ui" line="89"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="ConnectDialog.cpp" line="103"/>
        <source>Unsupported scheme.</source>
        <translation>Nicht unterstütztes URL-Schema.</translation>
    </message>
    <message>
        <location filename="ConnectDialog.cpp" line="117"/>
        <source>Connecting to %1...</source>
        <translation>Verbinde mit %1...</translation>
    </message>
    <message>
        <location filename="ConnectDialog.cpp" line="156"/>
        <source>Failed to connect to %1: %2.</source>
        <translation>Konnte nicht mit %1 verbinden: %2.</translation>
    </message>
</context>
<context>
    <name>DataModel</name>
    <message>
        <location filename="DataModel.cpp" line="366"/>
        <source>Sources</source>
        <translation>Datenquellen</translation>
    </message>
    <message>
        <location filename="DataModel.cpp" line="369"/>
        <source>Values</source>
        <translation>Werte</translation>
    </message>
</context>
<context>
    <name>DataNode</name>
    <message>
        <location filename="DataNode.cpp" line="158"/>
        <source>Variable subscription failed: %1</source>
        <translation>Abonnement der Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="204"/>
        <source>Path: %1</source>
        <translation>Pfad: %1</translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="210"/>
        <source>Period: </source>
        <translation>Periode: </translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="213"/>
        <source>%1 s</source>
        <oldsource>%1â¯s</oldsource>
        <translation>%1 s</translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="217"/>
        <source>%1 ms</source>
        <oldsource>%1â¯ms</oldsource>
        <translation>%1 ms</translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="221"/>
        <source>%1 µs</source>
        <oldsource>%1â¯Âµs</oldsource>
        <translation>%1 µs</translation>
    </message>
    <message>
        <location filename="DataNode.cpp" line="229"/>
        <source>Dimension: %1</source>
        <translation>Dimension: %1</translation>
    </message>
</context>
<context>
    <name>DataSource</name>
    <message>
        <location filename="DataSource.cpp" line="178"/>
        <source>%2 ⇄ %3 KB/s</source>
        <oldsource>%2 â %3 KB/s</oldsource>
        <translation>%2 ⇄ %3 KB/s</translation>
    </message>
    <message>
        <location filename="DataSource.cpp" line="533"/>
        <source>TestManager</source>
        <translation>Testmanager</translation>
    </message>
</context>
<context>
    <name>DialPlugin</name>
    <message>
        <location filename="plugins/DialPlugin.cpp" line="42"/>
        <source>Dial / Analog Gauge</source>
        <translation>Zeigerinstrument</translation>
    </message>
    <message>
        <location filename="plugins/DialPlugin.cpp" line="170"/>
        <source>Display</source>
        <translation>Anzeige</translation>
    </message>
    <message>
        <location filename="plugins/DialPlugin.cpp" line="184"/>
        <source>Setpoint</source>
        <translation>Sollwert</translation>
    </message>
    <message>
        <location filename="plugins/DialPlugin.cpp" line="221"/>
        <location filename="plugins/DialPlugin.cpp" line="244"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>DialPluginDialog</name>
    <message>
        <location filename="plugins/DialPluginDialog.ui" line="14"/>
        <source>Color Gradient Editor</source>
        <translation>Farbsverlaufs-Editor</translation>
    </message>
    <message>
        <location filename="plugins/DialPluginDialog.cpp" line="143"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="plugins/DialPluginDialog.cpp" line="146"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
</context>
<context>
    <name>DigitalPlugin</name>
    <message>
        <location filename="plugins/DigitalPlugin.cpp" line="39"/>
        <source>Digital Display</source>
        <translation>Digitalanzeige</translation>
    </message>
    <message>
        <location filename="plugins/DigitalPlugin.cpp" line="113"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>DoubleSpinBoxPlugin</name>
    <message>
        <location filename="plugins/DoubleSpinBoxPlugin.cpp" line="39"/>
        <source>Parameter Editor</source>
        <translation>Parameter-Editor</translation>
    </message>
    <message>
        <location filename="plugins/DoubleSpinBoxPlugin.cpp" line="109"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>GraphPlugin</name>
    <message>
        <location filename="plugins/GraphPlugin.cpp" line="39"/>
        <source>Graph</source>
        <translation>Graph</translation>
    </message>
    <message>
        <location filename="plugins/GraphPlugin.cpp" line="107"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>LedPlugin</name>
    <message>
        <location filename="plugins/LedPlugin.cpp" line="39"/>
        <source>LED Display</source>
        <translation>LED-Anzeige</translation>
    </message>
    <message>
        <location filename="plugins/LedPlugin.cpp" line="113"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>Legend</name>
    <message>
        <location filename="Legend.cpp" line="109"/>
        <source>Hide</source>
        <translation>Verbergen</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="110"/>
        <source>Del</source>
        <translation>Del</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="115"/>
        <source>Text alignment</source>
        <translation>Textausrichtung</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="119"/>
        <source>Left</source>
        <translation>Links</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="126"/>
        <source>Center</source>
        <translation>Zentriert</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="133"/>
        <source>Right</source>
        <translation>Rechts</translation>
    </message>
    <message>
        <location filename="Legend.cpp" line="140"/>
        <source>Edit stylesheet...</source>
        <translation>Stylesheet editieren...</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="MainWindow.ui" line="14"/>
        <source>Testmanager NG</source>
        <translation>Testmanager NG</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="49"/>
        <source>He&amp;lp</source>
        <translation>&amp;Hilfe</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="56"/>
        <source>&amp;Options</source>
        <translation>&amp;Optionen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="63"/>
        <source>Wi&amp;ndows</source>
        <translation>&amp;Fenster</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="68"/>
        <source>&amp;Data Sources</source>
        <translation>&amp;Datenquellen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="75"/>
        <source>&amp;File</source>
        <translation>&amp;Datei</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="79"/>
        <source>&amp;Recent files</source>
        <translation>&amp;Zuletzt bearbeitete Dateien</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="97"/>
        <source>Edit</source>
        <translation>Bearbeiten</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="140"/>
        <source>Data So&amp;urces</source>
        <translation>Daten&amp;quellen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="152"/>
        <source>Filter expression</source>
        <translation>Filterausdruck</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="187"/>
        <source>&amp;Property Editor</source>
        <translation>&amp;Eigenschafts-Editor</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="209"/>
        <source>P&amp;ython Shell</source>
        <translation>P&amp;ython-Kommandozeile</translation>
    </message>
    <message>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;fixed&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation type="obsolete">&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;fixed&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot;-qt-paragraph-type:empty; margin-top:0px; margin-bottom:0px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;&lt;br /&gt;&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="257"/>
        <source>Python S&amp;cript</source>
        <translation>Python-S&amp;cript</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="311"/>
        <source>&amp;New</source>
        <translation>&amp;Neu</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="314"/>
        <source>New layout</source>
        <translation>Neues Layout</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="323"/>
        <source>&amp;Save</source>
        <translation>&amp;Speichern</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="326"/>
        <source>Save layout</source>
        <translation>Layout speichern</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="335"/>
        <source>Sa&amp;ve As...</source>
        <translation>Speichern &amp;als...</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="338"/>
        <source>Save layout to another file</source>
        <translation>Layout in andere Datei speichern</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="347"/>
        <source>&amp;Close</source>
        <translation>S&amp;chließen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="350"/>
        <source>Close the application</source>
        <translation>Den Testmanager schließen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="359"/>
        <source>About &amp;Qt</source>
        <translation>Über &amp;Qt</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="368"/>
        <source>&amp;About Testmanager</source>
        <translation>Über den &amp;Testmanager</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="377"/>
        <source>&amp;Load...</source>
        <translation>&amp;Öffnen...</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="380"/>
        <source>Load layout</source>
        <translation>Layout laden</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="389"/>
        <source>&amp;Add data source...</source>
        <translation>Daten&amp;quelle hinzufügen...</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="436"/>
        <source>Add a new tab</source>
        <translation>Einen neuen Reiter hinzufügen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="484"/>
        <source>&amp;Copy</source>
        <translation>&amp;Kopieren</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="487"/>
        <source>Ctrl+C</source>
        <translation>Ctrl+C</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="502"/>
        <source>&amp;Paste</source>
        <translation>&amp;Einfügen</translation>
    </message>
    <message>
        <source>Paste</source>
        <translation type="obsolete">Einfügen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="505"/>
        <source>Ctrl+V</source>
        <translation>Ctrl+V</translation>
    </message>
    <message>
        <source>Add data source</source>
        <translation type="obsolete">Datenquelle hinzufügen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="392"/>
        <location filename="MainWindow.ui" line="395"/>
        <source>Add a data source</source>
        <translation>Eine Datenquelle hinzufügen</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="400"/>
        <source>&amp;Test</source>
        <translation>&amp;Test</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="403"/>
        <source>Start application test</source>
        <translation>Applikationstest starten</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="415"/>
        <source>&amp;Edit Mode</source>
        <translation>&amp;Editiermodus</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="418"/>
        <source>Toggle Edit Mode</source>
        <translation>Den Editiermodus umschalten</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="421"/>
        <source>E</source>
        <translation>E</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="433"/>
        <source>Add Tab</source>
        <translation>Reiter hinzufügen</translation>
    </message>
    <message>
        <source>Add a tab at the end</source>
        <translation type="obsolete">Fügt einen Karteireiter hinzu</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="439"/>
        <source>A</source>
        <translation>A</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="448"/>
        <source>&amp;Global stylesheet...</source>
        <translation>&amp;Globales Stylesheet...</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="451"/>
        <source>Edit the global stylesheet</source>
        <translation>Das globale Stylesheet editieren</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="454"/>
        <source>G</source>
        <translation>G</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="466"/>
        <source>&amp;Connect to data sources</source>
        <translation>Datenquellen &amp;verbinden</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="469"/>
        <source>Connect all data sources</source>
        <translation>Mit allen Datenquellen verbinden</translation>
    </message>
    <message>
        <location filename="MainWindow.ui" line="472"/>
        <source>Connect all referenced data sources</source>
        <translation>Mit allen Datenquellen verbinden</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="675"/>
        <source>Failed to open %1</source>
        <translation>Konnte %1 nicht öffnen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1000"/>
        <location filename="MainWindow.cpp" line="1050"/>
        <source>Testmanager Layouts (*.tml)</source>
        <translation>Testmanager-Layouts (*.tml)</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1001"/>
        <location filename="MainWindow.cpp" line="1051"/>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
    <message numerus="yes">
        <location filename="MainWindow.cpp" line="934"/>
        <source>Paste %Ln widgets</source>
        <translation>
            <numerusform>%Ln Widget einfügen</numerusform>
            <numerusform>%n Widgets einfügen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="590"/>
        <source>Failed to open %1.</source>
        <translation>Konnte %1 nicht öffnen.</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1178"/>
        <source>New Tab</source>
        <translation>Neuer Reiter</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1241"/>
        <source>Connect</source>
        <translation>Verbinden</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1246"/>
        <source>Disconnect</source>
        <translation>Trennen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1251"/>
        <location filename="MainWindow.cpp" line="1499"/>
        <source>Remove</source>
        <translation>Entfernen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1255"/>
        <source>Replace...</source>
        <oldsource>Replace</oldsource>
        <translation>Ersetzen...</translation>
    </message>
    <message>
        <source>Remove datasource</source>
        <translation type="obsolete">Datenquelle entfernen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1261"/>
        <source>Show messages...</source>
        <translation>Meldungen anzeigen...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1266"/>
        <source>Configure messages...</source>
        <translation>Meldungen konfigurieren...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1273"/>
        <source>Add datasource...</source>
        <translation>Datenquelle hinzufügen...</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1279"/>
        <source>Copy variable URL</source>
        <translation>Variablen-URL kopieren</translation>
    </message>
    <message numerus="yes">
        <location filename="MainWindow.cpp" line="1284"/>
        <source>Select %Ln subscriber(s)</source>
        <oldsource>Select %Ln subscribers</oldsource>
        <translation>
            <numerusform>%n Abonnenten auswählen</numerusform>
            <numerusform>%n Abonnenten auswählen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1292"/>
        <source>Expand complete subtree</source>
        <translation>Alles ausklappen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1457"/>
        <source>Reset</source>
        <translation>Zurücksetzen</translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="1506"/>
        <source>Clear list</source>
        <translatorcomment>Liste leeren</translatorcomment>
        <translation></translation>
    </message>
    <message>
        <location filename="MainWindow.cpp" line="184"/>
        <location filename="MainWindow.cpp" line="1646"/>
        <location filename="MainWindow.cpp" line="1648"/>
        <source>%1 KB/s</source>
        <oldsource>%1â¯KB/s</oldsource>
        <translation>%1 KB/s</translation>
    </message>
</context>
<context>
    <name>MessageDialog</name>
    <message>
        <location filename="MessageDialog.ui" line="20"/>
        <source>Configure Messages</source>
        <translation>Meldungen konfigurieren</translation>
    </message>
    <message>
        <location filename="MessageDialog.ui" line="29"/>
        <source>Plain messages file:</source>
        <translation>Meldungsdatei:</translation>
    </message>
    <message>
        <location filename="MessageDialog.ui" line="41"/>
        <source>Select...</source>
        <translation>Auswählen...</translation>
    </message>
    <message>
        <location filename="MessageDialog.cpp" line="65"/>
        <source>EtherLab PlainMessages files (*.xml)</source>
        <translation>EtherLab-Meldungen (*.xml)</translation>
    </message>
    <message>
        <location filename="MessageDialog.cpp" line="66"/>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>MessageWidget</name>
    <message>
        <location filename="MessageWidget.ui" line="14"/>
        <source>Messages</source>
        <translation>Meldungen</translation>
    </message>
</context>
<context>
    <name>Plugin</name>
    <message>
        <source>Not supported.</source>
        <translation type="obsolete">Nicht unterstützt.</translation>
    </message>
</context>
<context>
    <name>PropertyModel</name>
    <message>
        <location filename="PropertyModel.cpp" line="258"/>
        <source>Property</source>
        <translation>Eigenschaft</translation>
    </message>
    <message>
        <location filename="PropertyModel.cpp" line="260"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
</context>
<context>
    <name>PushButtonPlugin</name>
    <message>
        <location filename="plugins/PushButtonPlugin.cpp" line="39"/>
        <source>Push Button</source>
        <translation>Schaltfläche</translation>
    </message>
    <message>
        <location filename="plugins/PushButtonPlugin.cpp" line="109"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>RadioButtonPlugin</name>
    <message>
        <location filename="plugins/RadioButtonPlugin.cpp" line="39"/>
        <source>RadioButton</source>
        <translation>Auswahlfeld</translation>
    </message>
    <message>
        <location filename="plugins/RadioButtonPlugin.cpp" line="109"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>ReplaceDialog</name>
    <message>
        <location filename="ReplaceDialog.ui" line="20"/>
        <source>Replace data source...</source>
        <translation>Datenquelle ersetzen...</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.ui" line="49"/>
        <source>Rep&amp;lace during this session only</source>
        <translation>Nur in dieser &amp;Sitzung ersetzen</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.ui" line="59"/>
        <source>Replace &amp;permanently</source>
        <translation>&amp;Dauerhaft ersetzen</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.ui" line="99"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.ui" line="106"/>
        <source>Cancel</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.cpp" line="44"/>
        <source>New URL for datasource %1:</source>
        <translation>Neue URL für Datenquelle %1:</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.cpp" line="115"/>
        <source>Unsupported scheme.</source>
        <translation>Nicht unterstütztes URL-Schema.</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.cpp" line="129"/>
        <source>Connecting to %1...</source>
        <translation>Verbinde mit %1...</translation>
    </message>
    <message>
        <location filename="ReplaceDialog.cpp" line="168"/>
        <source>Failed to connect to %1: %2.</source>
        <translation>Konnte nicht mit %1 verbinden: %2.</translation>
    </message>
</context>
<context>
    <name>ScaleDelegate</name>
    <message>
        <location filename="ScaleDelegate.cpp" line="45"/>
        <source>1</source>
        <translation>1</translation>
    </message>
    <message>
        <location filename="ScaleDelegate.cpp" line="47"/>
        <source>rpm</source>
        <translatorcomment>Umdrehungen pro Minute</translatorcomment>
        <translation>1/min</translation>
    </message>
    <message>
        <location filename="ScaleDelegate.cpp" line="49"/>
        <source>°</source>
        <translation>°</translation>
    </message>
    <message>
        <location filename="ScaleDelegate.cpp" line="51"/>
        <source>bar</source>
        <translation>bar</translation>
    </message>
</context>
<context>
    <name>ScriptVariable</name>
    <message>
        <location filename="ScriptVariable.cpp" line="234"/>
        <source>Script variable subscription failed: %1</source>
        <translation>Abonnement der Script-Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="ScriptVariable.cpp" line="249"/>
        <source>Script variable poll failed: %1</source>
        <translation>Abruf der Script-Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="ScriptVariable.cpp" line="262"/>
        <source>URL not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>ScriptVariableModel</name>
    <message>
        <location filename="ScriptVariableModel.cpp" line="191"/>
        <source>Name</source>
        <translation>Name</translation>
    </message>
    <message>
        <location filename="ScriptVariableModel.cpp" line="194"/>
        <source>Period [s]</source>
        <translation>Periode [s]</translation>
    </message>
    <message>
        <location filename="ScriptVariableModel.cpp" line="197"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="ScriptVariableModel.cpp" line="200"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="ScriptVariableModel.cpp" line="307"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>SlotDialog</name>
    <message>
        <location filename="SlotDialog.ui" line="14"/>
        <source>Data connections</source>
        <translation>Datenverbindungen</translation>
    </message>
    <message>
        <location filename="SlotDialog.cpp" line="102"/>
        <source>Apply %1 to all</source>
        <translation>%1 auf alle anwenden</translation>
    </message>
    <message>
        <location filename="SlotDialog.cpp" line="109"/>
        <source>Remove variable</source>
        <translation>Variable entfernen</translation>
    </message>
    <message>
        <location filename="SlotDialog.cpp" line="115"/>
        <source>Jump to variable</source>
        <translation>Zur Variable springen</translation>
    </message>
</context>
<context>
    <name>SlotModel</name>
    <message>
        <location filename="SlotModel.cpp" line="267"/>
        <source>Variable</source>
        <translation>Variable</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="270"/>
        <source>Alias</source>
        <translation>Alias</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="273"/>
        <source>Period [s]</source>
        <oldsource>Period</oldsource>
        <translation>Periode [s]</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="276"/>
        <source>Scale</source>
        <translation>Skalierung</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="279"/>
        <source>Offset</source>
        <translation>Versatz</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="282"/>
        <source>LPF [s]</source>
        <oldsource>LPF</oldsource>
        <translation>Filterkonstante [s]</translation>
    </message>
    <message>
        <location filename="SlotModel.cpp" line="285"/>
        <source>Color</source>
        <translation>Farbe</translation>
    </message>
</context>
<context>
    <name>StyleDialog</name>
    <message>
        <location filename="StyleDialog.ui" line="14"/>
        <source>Edit Stylesheet</source>
        <translation>Stylesheet editieren</translation>
    </message>
    <message>
        <location filename="StyleDialog.ui" line="26"/>
        <source>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;See &lt;a href=&quot;http://doc.qt.io/qt-5/stylesheet-reference.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://doc.qt.io/qt-5/stylesheet-reference.html&lt;/span&gt;&lt;/a&gt; for a complete reference.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Use path prefix &amp;quot;layout:&amp;quot; to access paths relative to layout.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Tab page class name is &amp;quot;TabPage&amp;quot;.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</source>
        <translation>&lt;!DOCTYPE HTML PUBLIC &quot;-//W3C//DTD HTML 4.0//EN&quot; &quot;http://www.w3.org/TR/REC-html40/strict.dtd&quot;&gt;
&lt;html&gt;&lt;head&gt;&lt;meta name=&quot;qrichtext&quot; content=&quot;1&quot; /&gt;&lt;style type=&quot;text/css&quot;&gt;
p, li { white-space: pre-wrap; }
&lt;/style&gt;&lt;/head&gt;&lt;body style=&quot; font-family:&apos;Sans&apos;; font-size:10pt; font-weight:400; font-style:normal;&quot;&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Siehe &lt;a href=&quot;http://doc.qt.io/qt-5/stylesheet-reference.html&quot;&gt;&lt;span style=&quot; text-decoration: underline; color:#0000ff;&quot;&gt;http://doc.qt.io/qt-5/stylesheet-reference.html&lt;/span&gt;&lt;/a&gt; für die vollständige Referenz.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Das Pfad-Präfix „layout:“ erlaubt Pfade relativ zur Layout-Datei.&lt;/p&gt;
&lt;p style=&quot; margin-top:12px; margin-bottom:12px; margin-left:0px; margin-right:0px; -qt-block-indent:0; text-indent:0px;&quot;&gt;Der Klassenname des Reiter-Widgets ist „TabPage“.&lt;/p&gt;&lt;/body&gt;&lt;/html&gt;</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="47"/>
        <source>Add Resource...</source>
        <translation>Ressource hinzufügen...</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="48"/>
        <source>Add Color...</source>
        <translation>Farbe hinzufügen...</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="49"/>
        <source>Add Font...</source>
        <translation>Schriftart hinzufügen...</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="218"/>
        <source>Valid stylesheet</source>
        <translation>Gültiges Stylesheet</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="221"/>
        <source>Invalid stylesheet</source>
        <translation>Ungültiges Stylesheet</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="244"/>
        <source>Image files (*.png *.svg *.bmp)</source>
        <translation>Bild-Dateien (*.png *.svg *.bmp)</translation>
    </message>
    <message>
        <location filename="StyleDialog.cpp" line="245"/>
        <source>Any files (*)</source>
        <translation>Alle Dateien (*)</translation>
    </message>
</context>
<context>
    <name>SvgPlugin</name>
    <message>
        <source>Svg-Transformation</source>
        <translation type="vanished">SVG-Transformation</translation>
    </message>
    <message>
        <location filename="plugins/SvgPlugin.cpp" line="42"/>
        <source>Animated Vector Graphics</source>
        <translation>Animierte Vektorgrafik</translation>
    </message>
</context>
<context>
    <name>SvgPluginDialog</name>
    <message>
        <location filename="plugins/SvgPluginDialog.ui" line="14"/>
        <source>Svg Table Editor</source>
        <translation>SVG-Tabelleneditor</translation>
    </message>
    <message>
        <location filename="plugins/SvgPluginDialog.ui" line="33"/>
        <source>Path to svg file.</source>
        <translation>Pfad zu SVG-Datei.</translation>
    </message>
    <message>
        <source>No SVG-File selected!</source>
        <translation type="vanished">Keine SVG-Datei geladen!</translation>
    </message>
    <message>
        <location filename="plugins/SvgPluginDialog.cpp" line="115"/>
        <source>Attribute ID</source>
        <translation>Attribut-ID</translation>
    </message>
</context>
<context>
    <name>TabDialog</name>
    <message>
        <location filename="TabDialog.ui" line="14"/>
        <source>Edit Tab</source>
        <translation>Reiter editieren</translation>
    </message>
</context>
<context>
    <name>TabPage</name>
    <message>
        <location filename="TabPage.cpp" line="112"/>
        <source>Unknown widget type %1</source>
        <translation>Unbekannter Widget-Typ %1</translation>
    </message>
    <message numerus="yes">
        <location filename="TabPage.cpp" line="175"/>
        <source>Paste %Ln widgets</source>
        <translation>
            <numerusform>Widget einfügen</numerusform>
            <numerusform>%n Widgets einfügen</numerusform>
        </translation>
    </message>
    <message>
        <location filename="TabPage.cpp" line="196"/>
        <source>Edit stylesheet...</source>
        <translation>Stylesheet editieren...</translation>
    </message>
</context>
<context>
    <name>TextPlugin</name>
    <message>
        <location filename="plugins/TextPlugin.cpp" line="41"/>
        <source>Text Display</source>
        <translation>Textanzeige</translation>
    </message>
    <message>
        <location filename="plugins/TextPlugin.cpp" line="206"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>TextPluginDialog</name>
    <message>
        <location filename="plugins/TextPluginDialog.ui" line="14"/>
        <source>Text Table Editor</source>
        <oldsource>Text Hash Editor</oldsource>
        <translation>Text-Tabelle bearbeiten</translation>
    </message>
    <message>
        <location filename="plugins/TextPluginDialog.cpp" line="136"/>
        <source>Value</source>
        <translation>Wert</translation>
    </message>
    <message>
        <location filename="plugins/TextPluginDialog.cpp" line="139"/>
        <source>Text</source>
        <translation>Text</translation>
    </message>
</context>
<context>
    <name>TooltipMatrix</name>
    <message>
        <location filename="TooltipMatrix.cpp" line="82"/>
        <source>Variable subscription failed: %1</source>
        <translation>Abonnement der Variable fehlgeschlagen: %1</translation>
    </message>
    <message>
        <location filename="TooltipMatrix.cpp" line="96"/>
        <source>Variable polling failed: %1</source>
        <translation>Abfragen der Variable fehleschlagen: %1</translation>
    </message>
</context>
<context>
    <name>TouchEditPlugin</name>
    <message>
        <location filename="plugins/TouchEditPlugin.cpp" line="39"/>
        <source>Parameter Touch Editor</source>
        <translation>Parameter-Touch-Editor</translation>
    </message>
    <message>
        <location filename="plugins/TouchEditPlugin.cpp" line="109"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
<context>
    <name>WidgetContainer</name>
    <message>
        <location filename="WidgetContainer.cpp" line="502"/>
        <source>Property %1 does not exist.</source>
        <translation>Eigenschaft %1 existiert nicht.</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="509"/>
        <source>Failed to set property %1 to %2</source>
        <translation>Konnte Eigenschaft %1 nicht auf %2 setzen</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="608"/>
        <source>Syntax error in line %1: %2</source>
        <translation>Syntax-Fehler in Zeile %1: %2</translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="743"/>
        <source>Copy %Ln widget(s)</source>
        <translation>
            <numerusform>Widget kopieren</numerusform>
            <numerusform>%n Widgets kopieren</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="753"/>
        <source>Remove %Ln widget(s)</source>
        <translation>
            <numerusform>Widget entfernen</numerusform>
            <numerusform>%n Widgets entfernen</numerusform>
        </translation>
    </message>
    <message>
        <source>Clear process data</source>
        <translation type="obsolete">Prozessdaten entfernen</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="764"/>
        <source>Process data connections...</source>
        <translation>Datenverbindungen...</translation>
    </message>
    <message>
        <source>Clear process data connections</source>
        <translation type="obsolete">Datenverbindungen entfernen</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="770"/>
        <source>Jump to variable</source>
        <translation>Zur Variable springen</translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="788"/>
        <source>Clear process data connections of %Ln widget(s)</source>
        <translation>
            <numerusform>Datenverbindungen leeren</numerusform>
            <numerusform>Datenverbindungen von %n Widgets leeren</numerusform>
        </translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="797"/>
        <source>Align left</source>
        <translation>Linksbündig ausrichten</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="804"/>
        <source>Align center</source>
        <translation>Zentriert ausrichten</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="811"/>
        <source>Align right</source>
        <translation>Rechtsbündig ausrichten</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="827"/>
        <source>Send to back</source>
        <translation>Nach hinten</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="820"/>
        <source>Bring to front</source>
        <translation>Nach vorne</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="641"/>
        <source>Callable update() not found.</source>
        <translation>Aufrufbares update() nicht gefunden.</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="653"/>
        <source>update() is not callable.</source>
        <translation>update() ist nicht aufrufbar.</translation>
    </message>
    <message>
        <source>Error in excecution!</source>
        <translation type="obsolete">Fehler bei der Ausführung!</translation>
    </message>
    <message>
        <source>Edit stylesheet...</source>
        <translation type="obsolete">Stylesheet editieren...</translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="836"/>
        <source>Edit stylesheet of %Ln widget(s)...</source>
        <translation>
            <numerusform>Stylesheet editieren...</numerusform>
            <numerusform>Stylesheet von %n Widgets editieren...</numerusform>
        </translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="843"/>
        <source>Edit script...</source>
        <translation>Script bearbeiten...</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="849"/>
        <source>Show legend</source>
        <translation>Legende anzeigen</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="925"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="1005"/>
        <source>Process variables required.</source>
        <translation>Prozessdaten benötigt.</translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="1021"/>
        <source>Replace %Ln process variables.</source>
        <translation>
            <numerusform>Prozessvariable ersetzen.</numerusform>
            <numerusform>%n Prozessvariablen ersetzen.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="1024"/>
        <source>Replace %1.</source>
        <translation>Ersetze durch %1.</translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="1032"/>
        <source>%Ln slots required and %1 free</source>
        <translation>
            <numerusform>%n Slot benötigt, aber nur %1 frei</numerusform>
            <numerusform>%n Slots benötigt, aber nur %1 frei</numerusform>
        </translation>
    </message>
    <message numerus="yes">
        <location filename="WidgetContainer.cpp" line="1039"/>
        <source>Connect %Ln process variables.</source>
        <translation>
            <numerusform>Prozessvariable verbinden.</numerusform>
            <numerusform>%n Prozessvariablen verbinden.</numerusform>
        </translation>
    </message>
    <message>
        <location filename="WidgetContainer.cpp" line="1042"/>
        <source>Connect %1.</source>
        <translation>Verbinde mit %1.</translation>
    </message>
</context>
<context>
    <name>XYGraphPlugin</name>
    <message>
        <location filename="plugins/XYGraphPlugin.cpp" line="40"/>
        <source>XY Graph</source>
        <translation>X/Y-Graph</translation>
    </message>
    <message>
        <location filename="plugins/XYGraphPlugin.cpp" line="106"/>
        <source>Url not found: %1</source>
        <translation>URL nicht gefunden: %1</translation>
    </message>
</context>
</TS>

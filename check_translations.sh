#!/bin/bash

EXIT=0

for f in *.ts; do
    cp $f $f.precommit
done

lupdate-qt5 testmanager.pro

if ! grep -q 'type="unfinished"' *.ts; then
    echo "Found no unfinished translations."
else
    echo "Found UNFINISHED translations!"
    EXIT=1
fi

for f in *.ts; do
    mv $f.precommit $f
done

exit $EXIT

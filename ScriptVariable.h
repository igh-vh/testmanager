/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SCRIPTVARIABLE_H
#define SCRIPTVARIABLE_H

#include <pdcom/Variable.h>
#include <pdcom/Subscriber.h>

#include <QUrl>
#include <QIcon>

class ScriptVariableModel;
class DataModel;
class DataNode;
typedef struct _object PyObject;

/****************************************************************************/

class ScriptVariable:
    public QObject,
    public PdCom::Subscriber
{
    Q_OBJECT

    public:
        ScriptVariable(ScriptVariableModel *);
        virtual ~ScriptVariable();

        QUrl url;
        double period;
        QString name;

        QIcon getIcon() const;

        void fromJson(const QJsonValue &);
        QJsonValue toJson() const;

        void replaceUrl(const QUrl &, const QUrl &);

        bool hasData() const {
            return dataPresent;
        }

        double getValue() const {
            return value;
        }

        void connectVariables();

        void appendToPythonDict(PyObject *) const;

        static void registerPythonType();

    private:
        ScriptVariableModel * const model;
        DataNode *dataNode;
        PdCom::Variable *variable;
        bool dataPresent;
        double value;
        PyObject *pyObject;

        // virtual from PdCom::Subscriber
        void notify(PdCom::Variable *);
        void notifyDelete(PdCom::Variable *);

        ScriptVariable();
};

/****************************************************************************/

#endif

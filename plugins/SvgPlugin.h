/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SVGPLUGIN_H
#define SVGPLUGIN_H

/****************************************************************************/

#include "Plugin.h"

#include <QCoreApplication>

/****************************************************************************/

class SvgPlugin:
    public Plugin
{
    Q_DECLARE_TR_FUNCTIONS(SvgPlugin)

    public:
        // Plugin information
        QString name() const;
        QString type() const { return "QtPdWidgets::Svg"; }
        QIcon icon() const;

        // Private data
        void *createPrivateData(QWidget *) const;
        void readPrivateData(const QJsonObject &, QWidget *, void *) const;
        QJsonObject writePrivateData(QWidget *, const void *) const;
        void deletePrivateData(QWidget *, void *) const;
        void openEditor(QWidget*, void *, QWidget * = 0);

        // Process data connection
        int getSlotCapacity() const;
        int getFreeSlots(const SlotModel &) const;
        void appendVariable(QWidget *, SlotModel *, DataNode *) const;
        void connectDataSlots(QWidget *, const SlotModel *,
                const DataModel *) const;
        void clearVariables(QWidget *) const;

        struct Row {
            QString id;
            bool available;
        };

    private:
        QWidget *createWidget(QWidget *) const;
        static QStringList getIdList(const QList<Row> &);


};

/****************************************************************************/

#endif // SVGPLUGIN_H

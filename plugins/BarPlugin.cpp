/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "BarPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"

#include <QtPdWidgets/Bar.h>

#include <QJsonObject>

/****************************************************************************/

QString BarPlugin::name() const
{
    return tr("Bar");
}

/****************************************************************************/

QIcon BarPlugin::icon() const
{
    return QIcon(":/images/plugin-bar.svg");
}

/****************************************************************************/

int BarPlugin::getSlotCapacity() const
{
    return -1; // unlimited
}

/****************************************************************************/

int BarPlugin::getFreeSlots(const SlotModel &) const
{
    return -1; // unlimited
}

/****************************************************************************/

void BarPlugin::appendVariable(QWidget *widget, SlotModel *slotModel,
        DataNode *dataNode) const
{
    auto bar(dynamic_cast<Pd::Bar *>(widget));

    DataSlot *slot = new DataSlot(slotModel);
    slot->url = dataNode->nodeUrl();
    slot->period = 0.1;
    slot->color = slotModel->nextColor();

    PdCom::Variable *pv(dataNode->getVariable());
    if (pv) {
        bar->addVariable(pv, slot->period, slot->scale, slot->offset,
                slot->tau, slot->color);
        dataNode->notifySubscribed(slot->period);
    }
}

/****************************************************************************/

void BarPlugin::connectDataSlots(QWidget *widget,
        const SlotModel *slotModel, const DataModel *dataModel) const
{
    auto bar(dynamic_cast<Pd::Bar *>(widget));

    for (int i = 0; i < slotModel->getChildCount(); i++) {
        DataSlot *slot(dynamic_cast<DataSlot *>(slotModel->getChildNode(i)));
        if (!slot) {
            continue;
        }

        DataNode *dataNode(dataModel->findDataNode(slot->url));
        if (dataNode) {
            PdCom::Variable *pv(dataNode->getVariable());
            if (pv) {
                bar->addVariable(pv, slot->period, slot->scale,
                        slot->offset, slot->tau, slot->color);
                dataNode->notifySubscribed(slot->period);
            }
        }
        else {
            qWarning()
                << tr("Url not found: %1").arg(slot->url.toString());
        }
    }
}

/****************************************************************************/

void BarPlugin::clearVariables(QWidget *widget) const
{
    auto bar(dynamic_cast<Pd::Bar *>(widget));
    bar->clearVariables();
}

/****************************************************************************/

QWidget *BarPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Bar(parent);
}

/****************************************************************************/

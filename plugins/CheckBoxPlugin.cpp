/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "CheckBoxPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"

#include <QtPdWidgets/CheckBox.h>

#include <QJsonObject>

/****************************************************************************/

QString CheckBoxPlugin::name() const
{
    return tr("CheckBox");
}

/****************************************************************************/

QIcon CheckBoxPlugin::icon() const
{
    return QIcon(":/images/plugin-checkbox.svg");
}

/****************************************************************************/

int CheckBoxPlugin::getSlotCapacity() const
{
    return 1;
}

/****************************************************************************/

int CheckBoxPlugin::getFreeSlots(const SlotModel &slotModel) const
{
    int used = slotModel.getChildCount();
    return !used ? 1 : 0;
}

/****************************************************************************/

void CheckBoxPlugin::appendVariable(QWidget *widget, SlotModel *slotModel,
        DataNode *dataNode) const
{
    auto checkbox(dynamic_cast<Pd::CheckBox *>(widget));

    DataSlot *slot = new DataSlot(slotModel);
    slot->url = dataNode->nodeUrl();

    PdCom::Variable *pv(dataNode->getVariable());
    if (pv) {
        checkbox->setVariable(pv, slot->period, slot->scale, slot->offset,
                slot->tau);
        dataNode->notifySubscribed(slot->period);
    }
}

/****************************************************************************/

void CheckBoxPlugin::connectDataSlots(QWidget *widget,
        const SlotModel *slotModel, const DataModel *dataModel) const
{
    auto checkbox(dynamic_cast<Pd::CheckBox *>(widget));

    if (!slotModel->getChildCount()) {
        return;
    }

    DataSlot *slot(dynamic_cast<DataSlot *>(slotModel->getChildNode(0)));
    if (!slot) {
        return;
    }

    DataNode *dataNode(dataModel->findDataNode(slot->url));
    if (dataNode) {
        PdCom::Variable *pv(dataNode->getVariable());
        if (pv) {
            checkbox->setVariable(pv, slot->period, slot->scale, slot->offset,
                    slot->tau);
            dataNode->notifySubscribed(slot->period);
        }
    }
    else {
        qWarning()
            << tr("Url not found: %1").arg(slot->url.toString());
    }
}

/****************************************************************************/

void CheckBoxPlugin::clearVariables(QWidget *widget) const
{
    auto checkbox(dynamic_cast<Pd::CheckBox *>(widget));
    checkbox->clearVariable();
}

/****************************************************************************/

QWidget *CheckBoxPlugin::createWidget(QWidget *parent) const
{
    return new Pd::CheckBox(parent);
}

/****************************************************************************/

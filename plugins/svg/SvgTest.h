/******************************************************************************
 * SVG Plugin
 * Unittest to develope the integration of animated svg files to the qt
 * interface of the testmanager.
 *
 * Jonathan Grahl <jonathan.grahl@igh.de>
******************************************************************************/


// ----------------------------------------------------------------------------
#ifndef SVGTEST_H
#define SVGTEST_H

#include <QDateTime>
#include <QDomDocument>
#include <QFile>
#include <QFrame>
#include <QList>
#include <QSvgRenderer>
#include <QTimer>



// ============================================================================
class SvgPlugin:
    public QFrame
{
    Q_OBJECT

    // ------------------------------------------------------------------------
    public:
        SvgPlugin(QWidget * = 0);


    // ------------------------------------------------------------------------
    private slots:
        void updateTests();

    // ------------------------------------------------------------------------
    private:


        QTimer timer;
        int rep;
        int currentRep;
        int testID;
        int numTestcases;
        int flip;

        qint64 duration;

        QSvgRenderer renderer;
        QSvgRenderer varRenderer;

        QDomDocument staticDoc;
        QDomDocument varDoc;
        QDomDocument completeDoc;
        QDomElement groupElem;
        QDomElement childElem;

        QList<QDomNode> staticElements;
        QList<QDomNode> varElements;
        QDomElement msrGroup;
        QDomElement varElem;

        void createDocs(QList<QDomNode> &);
        void parseElements(QDomDocument &);

        void testRenderComplete();
        void testRenderDifferentiated();
        void changeColor();

        void paintEvent(QPaintEvent *);

};

#endif // SVGTEST_H

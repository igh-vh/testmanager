# -----------------------------------------------------------------------------
# SVG Plugin
# Unittest to develope the integration of animated svg files to the qt
# interface of the testmanager.
#
# Jonathan Grahl <jonathan.grahl@igh.de>
# -----------------------------------------------------------------------------


# -----------------------------------------------------------------------------
# General configuration
TEMPLATE = app
TARGET = unittest_svg



# -----------------------------------------------------------------------------
# Choosing qt engine
QT += xml svg
QMAKE_CXXFLAGS += -std=c++0x



# -----------------------------------------------------------------------------
# Specify project files and pathes
DEPENDPATH += .

OBJECTS_DIR = obj/
RCC_DIR = rcc/
MOC_DIR = moc/
UI_DIR = uic/

HEADERS += \
	SvgTest.h

SOURCES += \
	main.cpp \
	SvgTest.cpp



# -----------------------------------------------------------------------------
# Specify encoding
TRANSLATIONS = testmanager_de.ts
CODECFORTR = UTF-8

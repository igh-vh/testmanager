/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_DialPluginDialog.h"

#include "ColorDelegate.h"

#include <QtPdWidgets/Dial.h>

#include <QDialog>
#include <QAction>

/****************************************************************************/

class DialPluginDialog:
    public QDialog,
    public Ui::DialPluginDialog
{
    Q_OBJECT

    public:
        DialPluginDialog(QGradientStops *, QWidget * = 0);
        ~DialPluginDialog();

    private:
        QGradientStops * const stops;

        class Model;
        Model * const model;
        ColorDelegate colorDelegate;

    private slots:
        void on_buttonBox_accepted();
        void on_toolButtonAdd_clicked();
        void on_toolButtonRemove_clicked();
        void selectionChanged(const QItemSelection &, const QItemSelection &);
};

/****************************************************************************/

/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2018 - 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "ui_SvgPluginDialog.h"

#include "SvgPlugin.h"

#include <QtPdWidgets/Svg.h>

#include <QDialog>
#include <QAction>
#include <QItemDelegate>


/****************************************************************************/

class SvgPluginDialog:
    public QDialog,
    public Ui::SvgPluginDialog
{
    Q_OBJECT

    public:
        SvgPluginDialog(Pd::Svg &, QList<SvgPlugin::SvgPlugin::Row> &,
                QWidget * = 0);
        ~SvgPluginDialog();

    private:
        class Model;

        QList<SvgPlugin::Row> &svgAttributes;
        Model * const model;

        void setPathLabel();

    private slots:
        void on_buttonBox_accepted();
        void on_toolButtonAdd_clicked();
        void on_toolButtonRemove_clicked();
        void selectionChanged(const QItemSelection &, const QItemSelection &);
};

/****************************************************************************/

class SvgPluginDelegate:
    public QItemDelegate
{
    public:
        SvgPluginDelegate(QObject *parent = 0, QStringList list = QStringList());
        QWidget* createEditor(QWidget* , const QStyleOptionViewItem&,
                const QModelIndex&) const;

    private:
        QStringList stringList;

};

/****************************************************************************/

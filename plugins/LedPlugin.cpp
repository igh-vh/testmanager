/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#include "LedPlugin.h"

#include "DataSlot.h"
#include "DataModel.h"
#include "DataNode.h"
#include "SlotModel.h"

#include <QtPdWidgets/Led.h>

#include <QJsonObject>

/****************************************************************************/

QString LedPlugin::name() const
{
    return tr("LED Display");
}

/****************************************************************************/

QIcon LedPlugin::icon() const
{
    return QIcon(":/images/plugin-led.svg");
}

/****************************************************************************/

int LedPlugin::getSlotCapacity() const
{
    return 1;
}

/****************************************************************************/

int LedPlugin::getFreeSlots(const SlotModel &slotModel) const
{
    int used = slotModel.getChildCount();
    return !used ? 1 : 0;
}

/****************************************************************************/

void LedPlugin::appendVariable(QWidget *widget, SlotModel *slotModel,
        DataNode *dataNode) const
{
    auto digital(dynamic_cast<Pd::Led *>(widget));

    DataSlot *slot = new DataSlot(slotModel);
    slot->url = dataNode->nodeUrl();

    if (dataNode->getPeriod() > 0.0) {
        slot->period = 0.2;
    }

    PdCom::Variable *pv(dataNode->getVariable());
    if (pv) {
        digital->setVariable(pv, slot->period, slot->scale, slot->offset,
                slot->tau);
        dataNode->notifySubscribed(slot->period);
    }
}

/****************************************************************************/

void LedPlugin::connectDataSlots(QWidget *widget,
        const SlotModel *slotModel, const DataModel *dataModel) const
{
    auto digital(dynamic_cast<Pd::Led *>(widget));

    if (!slotModel->getChildCount()) {
        return;
    }

    DataSlot *slot(dynamic_cast<DataSlot *>(slotModel->getChildNode(0)));
    if (!slot) {
        return;
    }

    DataNode *dataNode(dataModel->findDataNode(slot->url));
    if (dataNode) {
        PdCom::Variable *pv(dataNode->getVariable());
        if (pv) {
            digital->setVariable(pv, slot->period, slot->scale, slot->offset,
                    slot->tau);
            dataNode->notifySubscribed(slot->period);
        }
    }
    else {
        qWarning()
            << tr("Url not found: %1").arg(slot->url.toString());
    }
}

/****************************************************************************/

void LedPlugin::clearVariables(QWidget *widget) const
{
    auto digital(dynamic_cast<Pd::Led *>(widget));
    digital->clearVariable();
}

/****************************************************************************/

QWidget *LedPlugin::createWidget(QWidget *parent) const
{
    return new Pd::Led(parent);
}

/****************************************************************************/

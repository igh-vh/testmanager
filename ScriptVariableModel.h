/*****************************************************************************
 *
 * Testmanager - Graphical Automation and Visualisation Tool
 *
 * Copyright (C) 2019  Florian Pose <fp@igh.de>
 *
 * This file is part of Testmanager.
 *
 * Testmanager is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * Testmanager is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for
 * more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with Testmanager. If not, see <http://www.gnu.org/licenses/>.
 *
 ****************************************************************************/

#ifndef SCRIPT_VARIABLE_MODEL_H
#define SCRIPT_VARIABLE_MODEL_H

/****************************************************************************/

#include "ScriptVariable.h"

#include <QAbstractTableModel>
#include <QJsonArray>

/****************************************************************************/

class WidgetContainer;
class DataModel;

/****************************************************************************/

class ScriptVariableModel:
    public QAbstractTableModel
{
    Q_OBJECT

    public:
        ScriptVariableModel(WidgetContainer *);
        ~ScriptVariableModel();

        void clear();
        bool isEmpty() const { return variables.isEmpty(); }

        void fromJson(const QJsonArray &);
        QJsonArray toJson() const;

        int rowCount(const QModelIndex &) const;
        int columnCount(const QModelIndex &) const;
        QVariant data(const QModelIndex &, int) const;
        QVariant headerData(int, Qt::Orientation, int) const;
        Qt::ItemFlags flags(const QModelIndex &) const;
        bool setData(const QModelIndex &, const QVariant &, int);

        QStringList mimeTypes() const;
        bool dropMimeData(const QMimeData *, Qt::DropAction, int, int,
                const QModelIndex &);

        void connectVariables();
        void replaceUrl(const QUrl &, const QUrl &);
        bool uses(const QUrl &) const;
        QSet<QUrl> getUrls() const;

        void notify(ScriptVariable *);

        WidgetContainer *getContainer() const { return container; }

        void appendToPythonDict(PyObject *) const;

        ScriptVariable *getScriptVariable(const QModelIndex &) const;

        void remove(ScriptVariable *);

    private:
        WidgetContainer * const container;
        QList<ScriptVariable *> variables;

        ScriptVariableModel();
};

/****************************************************************************/

#endif
